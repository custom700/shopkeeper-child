jQuery(document).ready(function($){
    jQuery('.nbd-swatch-wrap label').click(function(){
        jQuery(this).attr("title");
        var textColor = jQuery(this).attr("title");
        jQuery(".color-text .nbd-field-header>label").text('color: '+ textColor);
    });
    jQuery(document).on('click','.input-number-increment',function (e) {
        var quality = $(this).closest('.quantity-custom').find('.input-number');
        var quality_val = quality.val();
        var new_quality = parseInt(quality_val) + 1;
        quality.val(new_quality)
    });
    jQuery(document).on('click','.input-number-decrement',function (e) {
        var quality = $(this).closest('.quantity-custom').find('.input-number');
        var quality_val = quality.val();
        var new_quality = parseInt(quality_val) - 1;
        if(new_quality >= 0){
            quality.val(new_quality)
        }
    });
    // if($(".nbd-single-product-page").length > 0){
    //     jQuery('.quantity').css('display', 'none');
    //     if($('.nbo-bulk-variation').length > 0){
    //         $('.custom_button').addClass('custom_button_quantity');
    //     }
    //     if(jQuery('.quantity').length > 0){
    //         jQuery('.single_add_to_cart_button').css('display', 'none');
    //         // jQuery('.product_layout_classic form a.custom_button').addClass('custom_button_quantity');
    //     }
    // }

    if($(".custom_button").length > 0){
        var url         = $(".custom_button").attr("data-url")
        var id          = $(".single_add_to_cart_button").attr("data-product_id")
        var new_url     = url+id
        $(".custom_button").attr("href", new_url)
    }
});
